//
//  GameScene.swift
//  JPong
//
//  Created by Jamal on 6/20/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import SpriteKit
import GameplayKit

var currentGameType = GameType.easy

class GameScene: SKScene {
    
    var ball = SKSpriteNode()
    var friend = SKSpriteNode()
    var main = SKSpriteNode()
    
    var topLbl = SKLabelNode()
    var btmLbl = SKLabelNode()
    
    var score = [Int]() {
        didSet{
            topLbl.text = "\(score[1])"
            btmLbl.text = "\(score[0])"
        }
    }
    
    override func didMove(to view: SKView) {
        
        
        topLbl = childNode(withName: "topLabel") as! SKLabelNode
        btmLbl = childNode(withName: "bottomLabel") as! SKLabelNode
        
        ball = self.childNode(withName: "ball") as! SKSpriteNode
        friend = self.childNode(withName: "enemy") as! SKSpriteNode
        main = self.childNode(withName: "main") as! SKSpriteNode
        
        friend.position.y = (frame.height/2) - 50
        main.position.y = (-frame.height/2) + 50
        
        
        
        let border = SKPhysicsBody(edgeLoopFrom: frame)
        
        border.friction = 0
        border.restitution = 1
        
        physicsBody = border
        
        startGame()
        
    }
    
    func startGame(){
        score = [0, 0]
        ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: 10))
    }
    
    func addScore(playerWhoWon: SKSpriteNode){
        
        ball.position = CGPoint(x: 0, y: 0)
        ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        
        if playerWhoWon == main{
            score[0] += 1
            ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: 10))
        }
        else if playerWhoWon == friend{
            score[1] += 1
            ball.physicsBody?.applyImpulse(CGVector(dx: -10, dy: -10))
        }
        print(score)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches{
            let location = touch.location(in: self)
            
            if currentGameType == .player2{
                
                if location.y > 0{
                    friend.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                
                if location.y < 0{
                    main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                
            }
            else{
                main.run(SKAction.moveTo(x: location.x, duration: 0.2))
            }
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches{
            let location = touch.location(in: self)
            
            if currentGameType == .player2{
                
                if location.y > 0{
                    friend.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                
                if location.y < 0{
                    main.run(SKAction.moveTo(x: location.x, duration: 0.2))
                }
                
            }
            else{
                main.run(SKAction.moveTo(x: location.x, duration: 0.2))
            }
        }
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        
        switch currentGameType {
        case .easy:
            friend.run(SKAction.moveTo(x: self.ball.position.x, duration: 1.3))
            break
        case .medium:
            friend.run(SKAction.moveTo(x: self.ball.position.x, duration: 1.0))
            break
        case .hard:
            friend.run(SKAction.moveTo(x: self.ball.position.x, duration: 0.7))
            break
        case .player2:
            break
        }
        
        
        
        if self.ball.position.y <= main.position.y - 30{
            addScore(playerWhoWon: friend)
            
        }
        else if self.ball.position.y >=  friend.position.y + 30{
            addScore(playerWhoWon: main)
            
        }
        
    }
}
