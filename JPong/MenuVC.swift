//
//  MenuVC.swift
//  JPong
//
//  Created by Jamal on 6/22/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import UIKit

enum GameType{
    case easy
    case medium
    case hard
    case player2
}

class MenuVC: UIViewController{
    
    
    @IBAction func player2(_ sender: Any){
        moveToGame(type: .player2)
    }
    
    @IBAction func easy(_ sender: Any){
        moveToGame(type: .easy)
    }
    
    @IBAction func medium(_ sender: Any){
        moveToGame(type: .medium)
    }
    
    @IBAction func hard(_ sender: Any){
        moveToGame(type: .hard)
    }
    
    func moveToGame(type: GameType){
        let gameVC =  storyboard?.instantiateViewController(withIdentifier: "GameVC") as! GameViewController
        
        currentGameType = type
        
        navigationController?.pushViewController(gameVC, animated: true)
        
    }
    
}
